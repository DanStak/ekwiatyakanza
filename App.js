import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation'
import StackNav from './src/navigation/Navigator'


class App extends Component {
  render() {
    return <AppContainer />
  }
}


const AppContainer = createAppContainer(StackNav)

export default App;




import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import { Informations, Card, ProfilePhoto, UnderHeader } from './common';
import { users } from '../data/dummyData';


class Liked extends Component {

  state = {
    data: null
  }

  componentDidMount() {
    this.setState({
      data: users
    })
  }

  renderItem(user) {


    return (
      <Card>
        <ProfilePhoto
          photo={user.item.image}
        />
        <Informations
          name={user.item.name}
          date={user.item.date}
          value={user.item.value}
        />
      </Card>
    )
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#eeeeee', paddingBottom: 65 }}>


        <UnderHeader
          text='Dodaj z listy znajomych'
        />

        <View>
          <FlatList
            data={this.state.data}
            renderItem={this.renderItem}
            keyExtractor={user => user.id}
          />
        </View>

      </View>

    )
  }

};

// const styles = StyleSheet.create({
//   likeContainer: {
//     flex: 1,
//   }
// })


export default Liked;
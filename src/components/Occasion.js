import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { Category } from './common';


class Occasion extends Component {

  handlePress() {
    console.log('press!')
  };


  render() {

    return (
      <View style={{ flex: 1, backgroundColor: '#eeeeee' }}>

        <ScrollView >

          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>

            < Category
              text='Kwiaty w dniu Twojego świeta'
              price="od 107,91 zł"
              photo={require('../images/bouquet-3.jpg')}
              onPress={() => this.handlePress()}
            />
            <Category
              text='Kwiaty tylko Ty'
              price="od 91,90 zł"
              photo={require('../images/bouquet-1.jpg')}
              onPress={() => this.handlePress()}
            />
            <Category
              text='Kwiaty dla najpiękniejszej'
              price="od 125,90 zł"
              photo={require('../images/bouquet-2.jpg')}
              onPress={() => this.handlePress()}
            />
            <Category
              text='Bukiet od 5 do 200 róż'
              price="od 75 zł"
              photo={require('../images/bouquet-4.jpg')}
              onPress={() => this.handlePress()}
            />


          </View>

        </ScrollView >

      </View >
    );
  };
};


export default Occasion;





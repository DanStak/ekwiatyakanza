import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class Shop extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>SHOP</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eeeeee',
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    fontSize: 50
  }
})

export default Shop;
import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';


const Button = () => {
  return (
    <TouchableOpacity style={{ marginLeft: 20 }}>
      <Ionicons
        name="md-arrow-back"
        size={20} color='#63b501'
      />
    </TouchableOpacity>
  )
}

export default Button;
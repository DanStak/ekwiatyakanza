import React from 'react';
import { View, StyleSheet } from 'react-native';


const Card = (props) => {
  return (
    <View
      style={[styles.cardContainer, props.style]}
    >
      {props.children}
    </View>
  )
}

const styles = StyleSheet.create({
  cardContainer: {
    borderRadius: 10,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 16,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 20,
    paddingBottom: 20,
  }
})

export { Card };
import React from 'react';
import { Card, } from './Card';
import { LikedButton } from './LikedButton';
import { Text, StyleSheet, View, Image } from 'react-native';


const Category = (props) => {

  const { photo, text, price } = props
  return (
    <Card style={styles.category}>

      <View style={styles.viewImage}>
        <Image style={styles.image} source={photo} />
        <LikedButton onPress={props.onPress} />
      </View>


      <View style={styles.textView}>
        <Text style={styles.mainText}>{text}</Text>
        <Text style={styles.priceText}>{price}</Text>
      </View>

    </Card >
  );
};

const styles = StyleSheet.create({
  category: {
    marginBottom: 0,
    marginTop: 24,
    backgroundColor: '#fff',
    flexDirection: 'column',
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    paddingRight: 0,
    width: "41%",
    height: 250,
    justifyContent: 'space-around'
  },

  viewImage: {
    flex: 2,
    borderBottomWidth: 1,
    borderColor: '#eeeeee',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  image: {
    width: "100%",
    height: "100%",
    borderRadius: 10
  },

  textView: {
    flex: 1,
    width: '100%',
    padding: 20,
    justifyContent: 'space-between'
  },

  mainText: {
    fontSize: 8,
    color: '#3a3a3a',
  },

  priceText: {
    color: '#47366d',
    fontSize: 15,
    fontWeight: 'bold',
  }
})

export { Category };
import React from 'react';
import { View, Text, StyleSheet, Switch } from 'react-native';


const Informations = (props) => {
  return (
    <View style={styles.container} >
      <View>

        <Text
          style={[styles.nameText, props.value ? styles.textActive : null]}
        >
          {props.name}
        </Text>

      </View>

      <View style={styles.leftBottom}>

        <Text
          style={[styles.infoText, props.value ? styles.textActive : null]}
        >
          {props.date}
        </Text>

        <Text
          style={[styles.infoText, props.value ? styles.birthdayActive : null]}
        >
          Urodziny
        </Text>

        <Switch
          thumbColor="#fff"
          value={props.value}
        />

      </View>

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingRight: 23
  },

  nameText: {
    fontSize: 14,
    fontWeight: "bold",
    marginBottom: 10,
    color: '#cccccc',
  },

  textActive: {
    color: '#333333'
  },

  leftBottom: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  infoText: {
    color: '#cccccc',
    fontSize: 8
  },

  birthdayActive: {
    color: '#63b501',
  },

  informations: {
    marginLeft: 34,
  }
})

export { Informations };
import React from 'react';
import { TouchableOpacity, StyleSheet, Text, Image } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const LikedButton = (props) => {
  return (
    <TouchableOpacity
      style={styles.button}
      onPress={props.onPress}
    >
      <Ionicons name="md-heart-empty" size={20} color="#eff7e5" />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: 40,
    height: 40,
    borderRadius: 100,
    backgroundColor: '#63b501',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 10,
    bottom: 10
  },
})

export { LikedButton };
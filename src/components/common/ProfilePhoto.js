import React from 'react';
import { Image, View, StyleSheet } from 'react-native';

const ProfilePhoto = (props) => {
  return (
    <View style={styles.profilePhotoContainer}>
      <Image
        source={props.photo}
        style={styles.profilePhoto}
      />
    </View>

  )
};

const styles = StyleSheet.create({
  profilePhotoContainer: {
    marginLeft: 25,
    marginRight: 25,
  },

  profilePhoto: {
    width: 60,
    height: 60,
    borderRadius: 100,
  }
})

export { ProfilePhoto }
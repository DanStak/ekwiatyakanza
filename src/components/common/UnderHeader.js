import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const UnderHeader = (props) => {
  return (
    <View
      style={styles.underHeaderContainer}
    >
      <View style={styles.lines} />

      <Text style={styles.textHeader}>{props.text}</Text>

      <View style={styles.lines} />
    </View>
  );
};

const styles = StyleSheet.create({
  textHeader: {
    color: '#616161',
    fontSize: 10,
    paddingLeft: 9,
    paddingRight: 9
  },

  underHeaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 36,
    flexDirection: 'row',
    marginTop: 36
  },

  lines: {
    height: 1,
    width: 60,
    backgroundColor: '#cccccc',
  }
})

export { UnderHeader };
const bouquet4 = require("../images/bouquet-4.jpg");
const bouquet3 = require("../images/bouquet-3.jpg");
const bouquet2 = require("../images/bouquet-2.jpg");
const bouquet1 = require("../images/bouquet-1.jpg");

export const bouquet = [
  {
    image: bouquet3,
    text: 'Kwiaty w dniu Twojego świeta',
    price: 'od 107,91 zł',
    id: '1',
  },

  {
    image: bouquet1,
    text: 'Kwiaty tylko Ty',
    price: 'od 91,90 zł',
    id: '2',
  },

  {
    image: bouquet2,
    text: 'Kwiaty dla najpiękniejszej',
    price: 'od 125,90 zł',
    id: '3',
  },

  {
    image: bouquet4,
    text: 'Bukiet od 5 do 200 róż',
    price: 'od 75 zł',
    id: '4',
  }
]

export default bouquet;
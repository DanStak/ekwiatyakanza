const profile1 = require("../images/person-1.jpeg");
const profile2 = require("../images/person-2.jpg");

export const users = [
  {
    name: "Małgosia Kowalska",
    date: "07.05.2019",
    id: '1',
    image: profile2,
    value: true
  },

  {
    name: "Monika Matyj",
    date: "18.05.2019",
    id: '2',
    image: profile2,
    value: false
  },

  {
    name: "Wojciech Kowalski",
    date: "23.05.2019",
    id: '3',
    image: profile1,
    value: true
  },

  {
    name: "Klaudia Kołodziej",
    date: "25.05.2019",
    id: '4',
    image: profile2,
    value: false
  },

  {
    name: "Mateusz Sapiecha",
    date: "29.05.2019",
    id: '5',
    image: profile1,
    value: false
  }

]


export default users;
import React, { Component } from 'react';
import {
  createAppContainer,
  createBottomTabNavigator,
  createStackNavigator,
} from 'react-navigation';

import Liked from '../components/Liked';
import Occasion from '../components/Occasion';
import Promotions from '../components/Promotions';
import Shop from '../components/Shop';
import { Ionicons } from '@expo/vector-icons';

import Header from '../components/common/Header';
import Button from '../components/common/Button';


const BottomNav = createBottomTabNavigator(
  {

    Sklep: {
      screen: Shop,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Ionicons name="ios-cart" size={20} color={tintColor} />,
        headerTitle: 'logo'
      },
    },



    Promocje: {
      screen: Promotions,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Ionicons name="md-notifications" size={20} color={tintColor} />
      },
    },



    Okazje: {
      screen: Liked,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Ionicons name="md-calendar" size={20} color={tintColor} />
      },
    },



    Profil: {
      screen: Occasion,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Ionicons name="md-person" size={20} color={tintColor} />
      },
    }
  },



  {
    tabBarOptions: {
      activeTintColor: '#47366d',
      inactiveTintColor: '#8b8b8b',
      labelStyle: {
        fontSize: 12,
        paddingTop: 9
      },

      style: {
        paddingBottom: 13,
        paddingTop: 12,
        height: 65
      },
    }
  }
)


export const StackNav = createStackNavigator(
  {

    BottomNav: {
      screen: BottomNav,
      navigationOptions: {
        headerTitle: <Header />,
        headerLeft: <Button />,
        headerStyle: {
          height: 72,
        }
      },
    },
  },
  {
    headerLayoutPreset: 'center'
  }
)


export default StackNav